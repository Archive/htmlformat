#!/usr/bin/perl -w

require 5.004;
use strict;
use blib;

use HtmlFormat;

use vars qw[%CONFIG];

unless ($#ARGV >= 1) {
  die "Usage: $0 web-mirror-dir source-file [content-file]";
}

require $ARGV [0].'/site/local/local-config.pl';

my $hfm = new HtmlFormat ($ARGV [1]);

$hfm->parameter('source_file', $ARGV [1]);
$hfm->parameter('content_frame', 'content');
$hfm->parameter('navigation_frame', 'navbar');
$hfm->parameter('ignore_bg', ['content', 'logo']);
$hfm->parameter('doctype', 'HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"');
$hfm->parameter('frame_target_relative_to_input_dir', 0);
$hfm->parameter('frame_target', 'tables/test/foo/bar/');
$hfm->parameter('rewrite_nav_links', {'nav/nav.html' => ''});
$hfm->parameter('DOCROOT', $CONFIG{DOCROOT});

if ($#ARGV == 2) {
  $hfm->parameter('content_file', $ARGV [2]);
}

$hfm->init_hfm;
$hfm->output;
