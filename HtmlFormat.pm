package HtmlFormat;

use strict;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK @PARAMETER_NAMES);

require Exporter;
require AutoLoader;

require HTML::TreeBuilder;
require HTML::Element;
require Data::Dumper;
require Cwd;

@ISA = qw(Exporter AutoLoader);
# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.
@EXPORT = qw[];
@EXPORT_OK = qw[@PARAMETER_NAMES];
$VERSION = '0.01';

@PARAMETER_NAMES = qw[DOCROOT doctype content_frame navigation_frame
		      ignore_bg frame_target frame_target_relative_to_input_dir
		      rewrite_nav_links];

# Preloaded methods go here.

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self  = {};
  $self->{TreeBuilder} = new HTML::TreeBuilder;
  $self->{TreeBuilder}->strict_comment (1);
  $self->{TreeBuilder}->ignore_unknown (0);

  $self->{INPUTFILE} = shift;
  $self->{HEAD} = [];
  $self->{BODYATTRS} = {};
  $self->{PARAMETER} = {};
  bless ($self, $class);
  return $self;
}

# Copy important parameters to child object

sub copy_parameters {
  my ($self, $parent) = @_;

  foreach (@PARAMETER_NAMES) {
    $self->{PARAMETER}->{$_} = $parent->{PARAMETER}->{$_} unless
      defined $self->{PARAMETER}->{$_};
  }
}

# Return directory part of a filename

sub directory {
  my ($filename) = @_;
  
  $filename =~ s,^(.*/)[^/]*$,$1,;
  return $filename;
}

# Remove duplicate /'s etc.

sub normalize_dir {
  my ($self, $directory) = @_;
  
  $directory =~ s,//,/,g;
  $directory =~ s,//,/,g;
  $directory =~ s,/\./,/,g;
  $directory =~ s,/[^\.][^/]+/\.\./,/,g;
  return $directory;
}

# Strip directory prefix

sub strip_directory_prefix {
  my ($directory, $prefix) = @_;
  
  if (index ($directory, $prefix) == 0) {
    $directory = substr $directory, length $prefix;
  }
  
  return $directory;
}

sub find_file {
  my ($self, $filename) = @_;
  
  # Filenames starting with '/' are already absolute ones.
  return $filename if $filename =~ m,^/,;
  # If it's in the user's ~
  return $self->normalize_dir (getenv ('HOME').'/'.substr ($filename, 2))
    if $filename =~ m,^~,;
  # Filenames starting with '.' are interpreted as being relative to the
  # current directory.
  return $self->normalize_dir (Cwd::cwd.'/'.$filename) if $filename =~ m,^\.,;
  # Otherwise interpret them as being relative to the DOCROOT.
  return $self->normalize_dir ($self->{PARAMETER}->{DOCROOT}.'/'.$filename);
}

# Does the same as `mkdir -p'.

sub create_dir {
  my ($self, $directory) = @_;

  $directory = $self->find_file ($directory) unless $directory =~ m,^/,;  
  Carp::cluck "need an absolute directory" unless $directory =~ m,^/,;
  
  my $path = '/';
  my @dirs = split '/', $self->normalize_dir (directory ($directory));
  foreach my $dir (@dirs) {
    $path .= $dir.'/';
    unless (-d $path) {
      mkdir $path, 0755 or do {
	warn "mkdir ($path): $!";
	return;
      };
    }
  }
}

sub reverse_link {
  my ($self, $directory) = @_;

  $directory = $self->find_file ($directory) unless $directory =~ m,^/,;  
  Carp::cluck "need an absolute directory" unless $directory =~ m,^/,;
  
  my $path = '/';
  my @dirs = split '/', $self->normalize_dir (directory ($directory));

  my $dir;
  my $link = '';
  while (defined ($dir = pop @dirs)) {
    next if $dir eq '';
    next if $dir eq '.';

    if ($dir eq '..') {
      Carp::cluck "This should never happen.";
      next;
    };

    $link .= '../';
  }
  return $link;
}

sub init_hfm {
  my ($self) = @_;

  $self->{INPUTFILE} = $self->find_file ($self->{INPUTFILE});
  open INPUTFILE, $self->{INPUTFILE} or
    Carp::croak "open ($self->{INPUTFILE}): $!";
  while (defined ($_ = <INPUTFILE>)) {
    $self->{TreeBuilder}->parse ($_);
  }
  close INPUTFILE;
}

# Get/set parameters

sub parameter {
  my ($self,$name,$value) = @_;

  if (defined $value) {
    return $self->{PARAMETER}->{$name} = $value;
  } else {
    return $self->{PARAMETER}->{$name};
  }
}

# Call this after all parsing is done

sub content {
  my $self = shift;

  sub c_traverse {
    my ($self, $p) = @_;
    my ($tag) = $p->tag;

    if ($tag eq 'body') {
      foreach (qw[background bgcolor]) {
	my $attr = $p->attr($_);
	next unless defined $attr;
	$self->{BODYATTRS}->{$_} = $attr;
      }

      my $new = new HTML::Element 'div';
      foreach (@{$p->content}) {
	$new->push_content ($_);
      }
      return $new;
    }

    if ($tag eq 'head') {
      foreach (@{$p->content}) {
	push @{$self->{HEAD}}, $_;
      }
    }

    if ($tag eq 'html') {
      my $content = $p->content;
      
      foreach my $element (@$content) {
	next unless ref $element;

	my $ret = c_traverse ($self, $element);
	next unless ref $ret;
	return $ret;
      }
    }
  }

  return c_traverse ($self, $self->{TreeBuilder});
}

sub my_as_HTML
{
  my $self = shift;
  my $last = "\n";
  my @html = ();

  my %emptyElement = %HTML::Element::emptyElement;
  my %optionalEndTag = %HTML::Element::optionalEndTag;

  $self->traverse
    (
     sub {
       my($node, $start, $depth) = @_;
       if (ref $node) {
	 my $tag = $node->tag;
	 if ($start) {
	   my $prefix = '';
	   if (!(substr ($last, -1, 1) =~ /\n$/s)) {
	     $prefix = "\n".("  " x $depth);
	   }
	   $last = $prefix.$node->starttag;
	   push(@html, $last);
	 } elsif (not ($emptyElement{$tag} or $optionalEndTag{$tag})) {
	   my $prefix = '';
	   if (!(substr ($last, -1, 1) =~ /\n$/s)) {
	     $prefix = "\n".("  " x $depth);
	   }
	   $last = $prefix.$node->endtag;
	   push(@html, $last);
	 }
       } else {
	 # simple text content
	 HTML::Entities::encode_entities($node, "<>&");
	 push(@html, $node);
	 $last = $node;
       }
     }
    );
  join('', @html, "\n");
}

sub output {
  my $self = shift;

  # Clone an (normally HTML::Element) object using Data::Dumper and eval.

  sub clone {
    my ($dumpit) = @_;

    my $cloned;
    my $dumper = Data::Dumper->new ([$dumpit], [qw[cloned]]);
    $cloned = eval $dumper->Dump;
    return $cloned;
  }

  # Determine name of the output file.

  sub output_filename {
    my ($self, $output_name, $content_file) = @_;

    my $output_prefix_dir = (defined $content_file) ? $content_file :
      ((defined $self->{PARAMETER}->{content_file}) ?
       $self->{PARAMETER}->{content_file} : $self->{INPUTFILE});
    my $output_prefix = $output_prefix_dir;

    if (ref $self->{PARAMETER}->{rewrite_nav_links} and
	defined $self->{PARAMETER}->{rewrite_nav_links}->{$output_name}) {
      $output_name = $self->{PARAMETER}->{rewrite_nav_links}->{$output_name};
    }

    $output_prefix =~ s,^.*/([^/]*)$,$1,;
    $output_prefix =~ s,\..*$,,;
    $output_name =~ s,^.*/([^/]*)$,$1,;
    $output_name =~ s,\..*$,,;

    $output_prefix .= '_' unless $output_name eq '';
    
    $output_prefix_dir = directory ($output_prefix_dir);
    
    my $filename;
    if ($self->{PARAMETER}->{frame_target_relative_to_input_dir}) {
      $filename = $self->normalize_dir
	($output_prefix_dir.'/'.$self->{PARAMETER}->{frame_target}.
	 $output_prefix.$output_name.'.html');
    } else {
      $filename = $self->normalize_dir
	($self->{PARAMETER}->{DOCROOT}.'/'.$self->{PARAMETER}->{frame_target}.
	 strip_directory_prefix ($output_prefix_dir, $self->{PARAMETER}->{DOCROOT}).'/'.
	 $output_prefix.$output_name.'.html');
    }

    return $filename;
  }
  
  # Rewrite links (in the navigation section).

  sub link_subst {
    my ($parent, $self, $source, $is_navbar) = @_;

    $source =~ m,^(.*/)?([^/]*)$,;
    my ($src_dir, $src_file) = (defined $1 ? $1 : '',$2);

    my $output_dir = strip_directory_prefix
      (directory (output_filename ($parent, $source)),
       $parent->{PARAMETER}->{DOCROOT}) or do {
	 Carp::cluck "Output will be outside the DOCROOT";
	 return;
       };

    my $reverse_link = $parent->reverse_link ($output_dir);

    print STDERR "TEST E: |$output_dir|$reverse_link|$source|\n";

    $self->traverse
      (sub {
	 my ($node, $start, $depth) = @_;
	 return unless $start;
	 my $tag = $node->tag;

	 if ($tag eq 'img') {
	   my $src = $node->attr('src');
#	   print STDERR "TEST D: |$source|$reverse_link|$src_dir|$src|\n";
	   $src = $reverse_link.$src_dir.$src;
	   $node->attr('src',$src);
	 } elsif ($tag eq 'a') {
	   my $href = $node->attr('href');
	   my $target = $node->attr('target');
	   $target = '' unless defined $target;
	   $node->attr('target','_top');

	   return 1 if (!defined $href) or ($href =~ /:/);

	   if ($is_navbar) {
	     my $parent_file = defined $parent->{PARAMETER}->{content_file} ? 
	       $parent->{PARAMETER}->{content_file} :
		 $parent->{PARAMETER}->{source_file};
	     $parent_file =~ s,.*/([^/]*)$,$1,; $parent_file =~ s/\..*$//;
	     if ($target eq $parent->{PARAMETER}->{content_frame}) {
	       my $output_file = strip_directory_prefix
		 (output_filename ($parent, $source, $src_dir.$href),
		  $parent->{PARAMETER}->{DOCROOT});
	       print STDERR "TEST C: |$reverse_link|$output_dir|$src_dir|$href|$output_file|\n";
	       $href = $reverse_link.$output_file;
	     } else {
	       my $nav_link = $src_dir.$href;
	       print STDERR "TEST A: |$parent_file|$src_dir|$href|$nav_link| -> ";
	       if (ref $parent->{PARAMETER}->{rewrite_nav_links} and
		   defined $parent->{PARAMETER}->{rewrite_nav_links}->{$nav_link}) {
		 $nav_link = $parent->{PARAMETER}->{rewrite_nav_links}->{$nav_link};
	       }
	       $nav_link =~ s,^.*/([^/]*)$,$1,;
	       $nav_link =~ s,\..*$,,;
	       $parent_file .= '_' unless $nav_link eq '';
	       $href = $parent_file.$nav_link.'.html';
	       print STDERR "|$nav_link|$href|\n";
	     }
	   } elsif ($target eq '_top') {
#	     print STDERR "TEST B: |$src_dir|$href|\n";
	     $href = $reverse_link.$src_dir.$href;
	   }
	   $node->attr('href', normalize_dir ($parent,$href));
	 }
	 return 1;
       }, 1);
  }

  # Extract all navigation links from the navigation frame and
  # return them in an array.

  sub nav_links {
    my ($parent, $self, $source) = @_;

    $source =~ m,^(.*/)?([^/]*)$,;
    my ($src_dir, $src_file) = (defined $1 ? $1 : '',$2);
    my @links;

    $self->traverse
      (sub {
	 my ($node, $start, $depth) = @_;
	 return unless $start;
	 my $tag = $node->tag;
	 if ($tag eq 'a') {
	   my $href = $node->attr('href');
	   my $target = $node->attr('target');

	   # Only real navigation links.
	   unless (defined $target and $target =~ /top/) {
	     push @links, $src_dir.$href;
	   }
	 }
	 return 1;
       }, 1);

    return @links;
  }

  sub frameset {
    my ($self, $p) = @_;
    my ($parent, @rows, @cols);

    if (defined $p->attr('cols')) {
      $parent = new HTML::Element 'tr', 'valign' => 'top';
      @cols = split ',', $p->attr('cols');
    } elsif (defined $p->attr('rows')) {
      $parent = new HTML::Element 'table', 'width' => '100%';
      @rows = split ',', $p->attr('rows');
    }

    my @multi_parent = ([$parent]);

    my $create_new = sub {
      my ($parent) = @_;
      my $new;

      if (defined $p->attr('cols')) {
	$new = new HTML::Element 'td';
	if ($#cols >= 0) {
	  my $width = $cols [0];
	  if ($width eq '*') {
	    $width = '100%';
	  }
	  $new->attr('width', $width);
	}
	$parent->push_content ($new);
      } elsif (defined $p->attr('rows')) {
	my $new_parent = new HTML::Element 'tr';
	$new = new HTML::Element 'td';
	if ($#rows >= 0) {
	  my $height = $rows [0];
	  if ($height eq '*') {
	    $height = '100%';
	  }
	  $new->attr('height', $height);
	}
	$new_parent->push_content ($new);
	$parent->push_content ($new_parent);
      }

#      print STDERR "CREATE_NEW\n";
#      $new->dump;
#      print STDERR "CREATE_NEW_END\n";

      return $new;
    };

    my $create_new_done = sub {
      if (defined $p->attr('cols')) {
	shift @cols;
      } elsif (defined $p->attr('rows')) {
	shift @rows;
      }
    };

    sub second {
      my ($self, $element, $parent) = @_;

      my $nav_link = (defined $parent->[0]->[1]) ?
	$parent->[0]->[1] : '';

      my $name = $element->attr('name');
      
      my $is_content = (defined $self->{PARAMETER}->{content_frame} and
			$name eq $self->{PARAMETER}->{content_frame});

      my ($src, $path);
      
      if ($is_content and defined $self->{PARAMETER}->{content_file}) {
	$src = $path = $self->{PARAMETER}->{content_file};
      } else {
	$src = ($nav_link eq '') ? $element->attr('src') : $nav_link;
	$path = qq[$self->{PARAMETER}->{DOCROOT}/$src];
      }
      
      my $source = new HtmlFormat ($path);
      next unless defined $source;
      $source->copy_parameters ($self);
      $source->init_hfm;
      
      my $is_navbar = (defined $self->{PARAMETER}->{navigation_frame} and
		       $name eq $self->{PARAMETER}->{navigation_frame});

      my $new = $parent->[1];
      
      print STDERR "SOURCE: |$src|$path|\n";
      
      $new->push_content ($source->content);
      
      if (defined $self->{PARAMETER}->{content_frame} and
	  $name eq $self->{PARAMETER}->{content_frame}) {
	$self->{BODYATTRS} = $source->{BODYATTRS};
      }
      
      link_subst ($self, $source->content, $src, $is_navbar);
      
      if (ref $self->{PARAMETER}->{ignore_bg}) {
	my $ignore = 0;
	foreach (@{$self->{PARAMETER}->{ignore_bg}}) {
	  next unless $name eq $_;
	  $ignore = 1;
	}
	unless ($ignore) {
	  foreach (keys %{$source->{BODYATTRS}}) {
	    $new->attr($_, $source->{BODYATTRS}->{$_});
	  }
	}
      }
    }

    # First Loop: Collect all navigation frames into @multi_parent.
    
    foreach my $element (@{$p->content}) {
      next unless ref $element;
      my $tag = $element->tag;

      next unless $tag eq 'frame';

      my $src = $element->attr('src') or next;
      my $path = qq[$self->{PARAMETER}->{DOCROOT}/$src];
      
      my $source = new HtmlFormat ($path);
      next unless defined $source;
      $source->copy_parameters ($self);
      $source->init_hfm;

      my $name = $element->attr('name');
	
      my $is_navbar = (defined $self->{PARAMETER}->{navigation_frame} and
		       $name eq $self->{PARAMETER}->{navigation_frame});

      next unless $is_navbar;

      my @nav_links = nav_links ($self, $source->content, $src);
      if ($self->{PARAMETER}->{use_nav_name_for_index_file} or
	  $self->{PARAMETER}->{frame_target} eq '') {
	  unshift @nav_links, $src;
      } else {
	  unshift @nav_links, '';
      }

      foreach my $nav_link (@nav_links) {
	my $cloned_parent = clone ($parent);

	print STDERR "NAV_LINK: |$nav_link|".scalar @multi_parent."|\n";

	push @multi_parent, [$cloned_parent, $nav_link];
      }
    }

    # Second Loop: Actually create the frames.

    foreach my $element (@{$p->content}) {
      next unless ref $element;
      my $tag = $element->tag;

      if ($tag eq 'frameset') {
	my $frame_content = frameset ($self, $element);
	next unless ref $frame_content;

	my $first = shift @$frame_content;

	Carp::cluck 'This should never happen' if scalar @$frame_content;

	foreach my $this_parent (@multi_parent) {
	  &$create_new ($this_parent->[0])->push_content ($first->[0]);
	}
	&$create_new_done;

	next;
      }

      next unless $tag eq 'frame';

      foreach my $this_parent (@multi_parent) {
	my $new = &$create_new ($this_parent->[0]);
	second ($self, $element, [$this_parent, $new]);
      }
      &$create_new_done;
    }

#    Carp::cluck "Returning now ".scalar @multi_parent;

    return [@multi_parent];
  }

  sub traverse {
    my ($self, $p) = @_;
    my ($tag) = $p->tag;

    if ($tag eq 'body') {
      foreach (qw[background bgcolor]) {
	my $attr = $p->attr($_);
	next unless defined $attr;
	$self->{BODYATTRS}->{$_} = $attr;
      }

      my $new = new HTML::Element 'div';
      foreach (@{$p->content}) {
	$new->push_content ($_);
      }
      return $new;
    }

    if ($tag eq 'head') {
      foreach (@{$p->content}) {
	push @{$self->{HEAD}}, $_;
      }
    }

    if ($tag eq 'html') {
      my $content = $p->content;
      
      foreach my $element (@$content) {
	next unless ref $element;

	my $new = traverse ($self, $element);
	return $new if ref $new;
      }
    }

    if ($tag eq 'frameset') {
      my $frameset_content = frameset ($self, $p);
      return unless ref $frameset_content;

      my @tables;

      foreach my $content (@$frameset_content) {
	my $table = new HTML::Element 'table', 'width' => '100%';

#	print STDERR "BEGIN OF CONTENT DUMP\n";
#	$content->[0]->dump;
#	print STDERR "END OF CONTENT DUMP\n";

	$table->push_content ($content->[0]);
	push @tables, [$table, $content->[1], $content->[2]];
      }

      return [@tables];
    }
  }

  my $content_ref = traverse ($self, $self->{TreeBuilder});

  foreach my $content (@$content_ref) {
    my $body = new HTML::Element 'body', %{$self->{BODYATTRS}};
    $body->push_content ($content->[0]);
    
    my $head = new HTML::Element 'head';
     foreach (@{$self->{HEAD}}) {
      $head->push_content ($_);
    }
    
    my $html = new HTML::Element 'html';
    $html->push_content ($head);
    $html->push_content ($body);
    
    my $doctype;
    if (defined $self->{PARAMETER}->{doctype}) {
      $doctype = $self->{PARAMETER}->{doctype};
    } else {
      $doctype = 'HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"';
    }

    if (defined $content->[1]) {
      my $filename = output_filename ($self, $content->[1]);

      print STDERR "FILE: |$content->[1]|$filename|\n";

      $self->create_dir ($filename);

      open OUTPUTFILE, "> $filename" or do {
	warn "open ($filename): $!";
	next;
      };
      print OUTPUTFILE qq[<!DOCTYPE $doctype>\n].&my_as_HTML($html);
      close OUTPUTFILE;
    }
    
    # print qq[<!DOCTYPE $doctype>\n].&my_as_HTML($html);
  }
}

# Call equivalent methods in HTML::TreeBuilder

sub parse {
  my $self = shift;
  return $self->{TreeBuilder}->parse (@_);
}

sub as_HTML {
  my $self = shift;
  return $self->{TreeBuilder}->as_HTML (@_);
}

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is the stub of documentation for your module. You better edit it!

=head1 NAME

HtmlFormat - Convert framed HTML pages to tabled HTML pages

=head1 SYNOPSIS

  use HtmlFormat;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for HtmlFormat was created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head1 AUTHOR

Martin Baulig <martin@home-of-linux.org>.

=head1 SEE ALSO

perl(1).

=cut
